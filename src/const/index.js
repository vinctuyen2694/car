const HOTLINE = "0938 901 015";
const PRODUCTS_KIA = [
  {
    id: 0,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/kia_new_Fronter/K200.png",
    alt: "xe tai thaco long bien",
    name: "KIA FRONTIER K200",
    weight: "1,99 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 1,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/kia_new_Fronter/K250.png",
    alt: "xe tai thaco long bien",
    name: "KIA FRONTIER K250",
    weight: "2,49 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 2,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/kia_new_Fronter/K200S/4WD/Icon-K200s-4wd.png",
    alt: "xe tai thaco long bien",
    name: "KIA K200S-4WD",
    weight: "1,49 Tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 3,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/kia_new_Fronter/K200S/2WD/Icon-K200s-2wd.png",
    alt: "xe tai thaco long bien",
    name: " KIA K200S-2WD",
    weight: "1,49 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 4,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/kia_new_Fronter/K200.png",
    alt: "xe tai thaco long bien",
    name: " KIA K200SD-4WD",
    weight: "1,49 Tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 5,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/kia_new_Fronter/K250L/ICON%20K250L.png",
    alt: "xe tai thaco long bien",
    name: "KIA FRONTIER K250L",
    weight: " 2.35 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 6,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/kia_new_Fronter/K250L/ICON%20K250L.png",
    alt: "xe tai thaco long bien",
    name: "FRONTIER K250B",
    weight: " 1.99 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
];
const PRODUCTS_THACO = [
  {
    id: 0,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/thaco_towner/Towner800A/TOWNER-800A_icon.png",
    alt: "xe tai thaco long bien",
    name: "THACO TOWNER 800A",
    weight: " 1,99 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 1,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/thaco_towner/TOWNER-990b.png",
    alt: "xe tai thaco long bien",
    name: "THACO TOWNER 990",
    weight: " 2,09 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 2,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/thaco_towner/TOWNER-800A-BC/TOWNER-800A-BC_icon.png",
    alt: "xe tai thaco long bien",
    name: "TOWNER 800A-BC",
    weight: " 1,99 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 3,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/thaco_towner/TOWNER-VAN/Icon-towner-van-2snew.jpg",
    alt: "xe tai thaco long bien",
    name: "TOWNER VAN 2S",
    weight: " 2.09 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 4,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/thaco_towner/TOWNER-VAN/Icon-towner-van-5s.jpg",
    alt: "xe tai thaco long bien",
    name: "TOWNER VAN 5S",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
];
const PRODUCTS_FOTON = [
  {
    id: 0,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/FOTON%20OLLIN%20S/ICON_OLLIN_S490.png",
    alt: "xe tai thaco long bien",
    name: "FOTON OLLIN S490",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 1,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/FOTON%20OLLIN%20S/S700/ICON_OLLIN_S100.png",
    alt: "xe tai thaco long bien",
    name: "FOTON OLLIN S700",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 2,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/FOTON%20OLLIN%20S/ICON_OLLIN_S720.png",
    alt: "xe tai thaco long bien",
    name: "FOTON OLLIN S720",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 3,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Ollin/2021/Thang5/ICON_OLLIN_490_600x330.png",
    alt: "xe tai thaco long bien",
    name: "FOTON OLLIN 490",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 4,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Ollin/2021/Thang5/ICON_OLLIN_500_600x330.png",
    alt: "xe tai thaco long bien",
    name: "FOTON OLLIN 500",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 5,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Ollin/2021/Thang5/ICON_OLLIN_120_600x330.png",
    alt: "xe tai thaco long bien",
    name: "FOTON OLLIN 120",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 6,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/foton_auman/C160_2.png",
    alt: "xe tai thaco long bien",
    name: "FOTON Auman C160",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 7,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/foton_auman/C240_2.png",
    alt: "xe tai thaco long bien",
    name: "FOTON Auman C240",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
];
const PRODUCTS_FUSO = [
  {
    id: 0,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Fuso/Canter-TF/TF49/fuso_Canter_TF_49_600x330_vi.png",
    alt: "xe tai thaco long bien",
    name: "FUSO Canter TF4.9",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 1,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Fuso/Canter-TF/TF75/fuso_Canter_TF_75_600x330_vi.png",
    alt: "xe tai thaco long bien",
    name: "FUSO Canter TF7.5",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 2,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Fuso/Canter-TF/TF85L/fuso_Canter_TF_85L_600x330_vi.png",
    alt: "xe tai thaco long bien",
    name: "FUSO Canter TF8.5",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 3,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Fuso/2020/Canter/canter499a.png",
    alt: "FUSO Canter TF4.99",
    name: "FOTON OLLIN 490",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 4,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Fuso/2020/Canter/canter65b.png",
    alt: "xe tai thaco long bien",
    name: "FUSO Canter TF6.5",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 5,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Fuso/2020/FA/icon_FA140.png",
    alt: "xe tai thaco long bien",
    name: "FUSO Canter FA 140",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 6,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Fuso/FJ/2020/icon_FJ1285.png",
    alt: "xe tai thaco long bien",
    name: "FUSO Canter FJ 285",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
  {
    id: 7,
    image:
      "https://thacocv.thacotai.vn//Content/UserFiles/Images/Products/Fuso/2020/FI/icon_FI170.png",
    alt: "xe tai thaco long bien",
    name: "FUSO Canter FA 170",
    weight: " 2,19 tấn",
    price: "Giá từ: 315.000.000 VND",
    promotion: "Ưu Đãi Giá Tốt Nhất, Tặng Option, Phụ Kiện",
  },
];
export { HOTLINE, PRODUCTS_KIA, PRODUCTS_THACO, PRODUCTS_FOTON, PRODUCTS_FUSO };
