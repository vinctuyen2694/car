import Vue from "vue";
import Router from "vue-router";
import AppHeader from "./layouts/AppHeader";
import AppFooter from "./layouts/AppFooter";
import Components from "./views/Components.vue";
import Landing from "./views/Landing.vue";
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
import Profile from "./views/Profile.vue";
// main code
import Home from "./views/Home.vue";
import Product from "./views/Product.vue";
import Contact from "./views/Contact.vue";
import ContributionProcedure from "./views/ContributionProcedure.vue";
import PriceCar from "./views/PriceCar.vue";

Vue.use(Router);

export default new Router({
  // linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home,
      meta: {
        layout: "default",
      },
    },
    {
      path: "/product/:id",
      name: "Product",
      component: Product,
      meta: {
        layout: "default",
      },
    },
    {
      path: "/contact",
      name: "Contact",
      component: Contact,
      meta: {
        layout: "default",
      },
    },
    {
      path: "/thu-tuc-tra-gop",
      name: "ContributionProcedure",
      component: ContributionProcedure,
      meta: {
        layout: "default",
      },
    },
    {
      path: "/bang-gia-xe",
      name: "PriceCar",
      component: PriceCar,
      meta: {
        layout: "default",
      },
    },
    // code demo
    {
      path: "/components",
      name: "components",
      components: {
        header: AppHeader,
        default: Components,
        footer: AppFooter,
      },
    },
    {
      path: "/landing",
      name: "landing",
      components: {
        header: AppHeader,
        default: Landing,
        footer: AppFooter,
      },
    },
    {
      path: "/login",
      name: "login",
      components: {
        header: AppHeader,
        default: Login,
        footer: AppFooter,
      },
    },
    {
      path: "/register",
      name: "register",
      components: {
        header: AppHeader,
        default: Register,
        footer: AppFooter,
      },
    },
    {
      path: "/profile",
      name: "profile",
      components: {
        header: AppHeader,
        default: Profile,
        footer: AppFooter,
      },
    },
  ],
  scrollBehavior: (to) => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  },
});
