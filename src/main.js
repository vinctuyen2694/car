import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Argon from "./plugins/argon-kit";
import "./registerServiceWorker";
import { VueExtendLayout, layout } from "vue-extend-layout";
import VueCarousel from "vue-carousel";

Vue.config.productionTip = false;
Vue.use(Argon);
Vue.use(VueCarousel);
new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
